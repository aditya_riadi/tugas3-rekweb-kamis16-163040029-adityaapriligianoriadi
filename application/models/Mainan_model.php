<?php

class Mainan_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

    public function getData($id = null){
        $this->db->select("*");
        $this->db->from("mainan");
        if ($id == null) {
            $this->db->order_by('id_mainan', 'asc');   
        }else{
            $this->db->where('id_mainan', $id);
        }
        return $this->db->get();
    }

    public function cari($cari)
    {
        $this->db->select("*");
        $this->db->from('mainan');
        $this->db->like('nama_mainan', $cari);
        return $this->db->get();


    }

    public function insert($data){
        $this->db->insert('mainan', $data);
        return $this->db->affected_rows();
    }

    public function update($table, $data, $par, $var) {
        $this->db->update($table, $data, array($par => $var));
        return $this->db->affected_rows();        
    }
    
    public function delete($table, $par, $var){
        $this->db->where($par, $var);
        $this->db->delete($table);
        return $this->db->affected_rows();
    }
}