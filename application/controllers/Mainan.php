<?php

require APPPATH . '/libraries/REST_Controller.php';

class Mainan extends REST_Controller
{

    public function __construct($config = "rest")
    {
        parent::__construct($config);
        $this->load->model("Mainan_model", "mainan");
    }

    public function index_get()
    {
        $id = $this->get('id_mainan');
        $cari = $this->get('cari');

        if($cari != ""){
            $mainan = $this->mainan->cari($cari)->result();
        }else if($id == ""){
            $mainan = $this->mainan->getData(null)->result();
        }else{
            $mainan = $this->mainan->getData($id)->result();
        }

        $this->response($mainan);
    }

    public function index_put()
    {
        $nama_mainan = $this->put('nama_mainan');
        $harga_mainan = $this->put('harga_mainan');
        $stok_mainan = $this->put('stok_mainan');
        $deskripsi_mainan = $this->put('deskripsi_mainan');
        $img_mainan = $this->put('img_mainan');
        $id = $this->put('id_mainan');
        $data = array(
            'nama_mainan' => $nama_mainan,
            'harga_mainan' => $harga_mainan,
            'stok_mainan' => $stok_mainan,
            'deskripsi_mainan' => $deskripsi_mainan,
            'img_mainan' => $img_mainan
        );
        
        $update = $this->mainan->update('mainan', $data, 'id_mainan', $this->put('id_mainan'));

        if ($update) {
            $this->response(array('status'=>'success', 200));
        }else{
            $this->response(array('status'=>'fail', 502));            
        }

    }

    public function index_post()
    {
        $nama_mainan = $this->post('nama_mainan');
        $harga_mainan = $this->post('harga_mainan');
        $stok_mainan = $this->post('stok_mainan');
        $deskripsi_mainan = $this->post('deskripsi_mainan');
        $img_mainan = $this->post('img_mainan');
        $id = $this->post('id_mainan');
        $data = array(
            'nama_mainan' => $nama_mainan,
            'harga_mainan' => $harga_mainan,
            'stok_mainan' => $stok_mainan,
            'deskripsi_mainan' => $deskripsi_mainan,
            'img_mainan' => $img_mainan
        );

        $insert = $this->mainan->insert($data);
        if ($insert) {
            $this->response(array('status'=>'success', 200));
        }else{
            $this->response(array('status'=>'fail', 502));            
        }
    }

    public function index_delete()
    {
        $id = $this->delete('id_mainan');
        $delete = $this->mainan->delete('mainan','id_mainan', $id);
        if ($delete) {
            $this->response(array('status'=>'success'), 201);
        }else{
            $this->response(array('status'=>'fail', 502));            
        }
    }
}